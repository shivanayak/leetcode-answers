package practice;

import java.util.HashMap;
import java.util.Map;

public class TwoSum {

	public static void main(String[] args) {
		int[] nums = {1 , 4 , 5 , 11 , 12};
		int target = 13;
		for (int x : twosum(nums, target)) {
			System.out.println(x);
		}

	}

	private static int[] twosum(int[] nums, int target) {
		Map<Integer,Integer> lookup = new HashMap<>();
		
		for(int i =0; i <nums.length; i++) {
			if(lookup.containsKey(target - nums[i])) {
				return new int[] {lookup.get(target - nums[i]),i};
			}
			lookup.put(nums[i], i);
		}
		
		return null;
	}

}
